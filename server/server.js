var express = require('express');
var app = express();
var operators= [
    { name : 'Operator 1',
      location : 'Tafa',
      state: 'Kaduna',
      id: 'operator1'
    },
    { name : 'Operator 2',
      location : 'Okene',
      state: 'Kogi',
      id: 'operator2'
    },
    { name : 'Operator 3',
      location : 'Oshogbo',
      state: 'Osun',
      id: 'operator3'
    },
    { name : 'Operator 4',
      location : 'Nsukka',
      state: 'Enugu',
      id: 'operator4'
    }

]

var vehicleClasses=[{type :'Motorcycle', charge : 100},{type: 'Car',charge: 200},{type: 'Van',charge: 400},{type: 'Lorry',charge:700}]
var auditVehicleReportQueue=[]
var operatorVehicleReportQueue=[]
var paymentQueue=[]
const pickRandom = require('pick-random');

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
function  generateTraffic()
{
    if(auditVehicleReportQueue.length < 5000)
    {
	
	var report={ vehicleClass: pickRandom(vehicleClasses),
		     operator:pickRandom(operators),
		     timestamp:new Date().toISOString()}
   
	auditVehicleReportQueue.push(report);
	if(getRandomInt(0,1000)%2 == 0)
	{
	    operatorVehicleReportQueue.push(report);
	
	}
    }
    
}

setInterval(generateTraffic,30000);

function getTotalAuditQueue()
{
    var sum=0;
    auditVehicleReportQueue.map((report)=>{

	sum+=report.vehicleClass.charge;
    });
    return sum;    
}
function getTotalOperatorQueue()
{
    var sum=0;
    operatorVehicleReportQueue.map((report)=>{

	sum+=report.vehicleClass.charge;
    });
    return sum;    
}

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var balance=10000;
app.get('/get/operators', function (req, res) {
    res.send(JSON.stringify(operators));
    
});

app.get('/get/auditQueue', function (req, res) {
    res.send(JSON.stringify(auditVehicleReportQueue));
    
});


app.get('/get/operatorQueue', function (req, res) {
    res.send(JSON.stringify(operatorVehicleReportQueue));
    
});



app.get('/put/:cardsn', function (req, res) {
    cardsn = req.params.cardsn;
    balance-=200;
    res.send('Good');
    console.log('CardSN:'+req.params.cardsn);
    var report={vehicleClass: {type :'car',charge: 200},
		operator:operators[0],
		timestamp:new Date().toISOString()}
    auditVehicleReportQueue.push(report);
    operatorVehicleReportQueue.push(report);
    
});

app.get('/charge/:amount', function (req, res) {
    var amount = req.params.amount;
    balance-=parseInt(amount);
    res.send(amount);
    console.log('Charged :'+req.params.amount);
});


app.get('/balance', function (req, res) {
    res.send(balance.toString());
    console.log('Balance is :'+balance);
});

app.get('/pay/:amount', function (req, res) {
    var amount = req.params.amount;
    balance+=parseInt(amount);
    res.send(balance.toString());
    console.log('Balance is :'+balance);
});


var server=app.listen(3000, function () {
    console.log('Express app listening on port 3000!')
})



