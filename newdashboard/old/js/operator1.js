function countvehicle(array,itemName){

  var count = 0;
  var charge=0;
for(var i = 0; i < array.length; ++i){
  
    if(array[i]['vehicleClass'][0]['type'] == itemName)
    {
        count++;
        charge=array[i]['vehicleClass'][0]['charge'];
    }
}
return count*charge;
}


var operatorData=[]
var auditData=[]
var operatorUrl= 'http://139.59.172.24:3000/get/operatorQueue';
var auditUrl= 'http://139.59.172.24:3000/get/auditQueue';

 
    function plotter()
    {




     var   operatorLorryNum = countvehicle(operatorData,'Lorry');
     var   auditLorryNum = countvehicle(auditData,'Lorry');
      console.log('operator '+operatorLorryNum+' audit'+auditLorryNum);

    var ctx = $('#barChart');
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Operator Lorry", "Audit Lorry", "Operator Van", "Audit Van", "Operator Motorcycle", "Audit Motorcycle", "Operator Car", "Audit Car"],
        datasets: [{
            label: '# of Vehicles',
            data: [  auditLorryNum, operatorLorryNum,countvehicle(auditData,'Van'), countvehicle(operatorData,'Van'), countvehicle(auditData,'Motorcycle'), countvehicle(operatorData,'Motorcycle'),countvehicle(auditData,'Car'),countvehicle(operatorData,'Car')],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(75, 192, 192, 1)',
                'rgba(75, 192, 192, 1)',
                
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
        
       
      // Pie chart


    var myChart = new Chart($('#pieChart'), {
    type: 'pie',
    data: {
    labels: ["Lorry", "Van",  "Motorcycle",  "Car"],
        datasets: [{
            label: '# of Vehicles',
            data: [  countvehicle(auditData,'Lorry'),countvehicle(auditData,'Van'), countvehicle(auditData,'Motorcycle'),countvehicle(auditData,'Car')],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
               
                'rgba(255, 206, 86, 0.2)',
             
                'rgba(75, 192, 192, 0.2)',
              
                'rgba(75, 192, 192, 1)',
              
                
            ]
           
        }]
    }
    
});
       
    

    }

var Lorry = (function() {

  var LorryNum;
      $.get( operatorUrl, function( data ) {

        operatorData=data;
          $.get( auditUrl, function( data ) {

                   auditData=data;
                    plotter();
          })

        
     })
})();
