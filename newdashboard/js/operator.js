
var operatorName = sessionStorage.getItem('operator');

function operatorVehicle(array,itemName){

  var count = 0;
  var charge=0;
for(var i = 0; i < array.length; ++i){
  
    if( array[i]['operator'][0]['id'] == operatorName && array[i]['vehicleClass'][0]['type'] == itemName)
    {
        count++;
        charge=array[i]['vehicleClass'][0]['charge'];
    }
}
return count*charge;
}


var operatorData=[]
var auditData=[]
var operatorUrl= 'http://139.59.172.24:3000/get/operatorQueue';
var auditUrl= 'http://139.59.172.24:3000/get/auditQueue';
var operatorsUrl ="http://139.59.172.24:3000/get/operators";
 function displayOperator(id)
 {

sessionStorage.setItem("operator",id);
location.href="operator.html";


 }
    function plotter()
    {






            function getoperators(){
                          $.get( operatorsUrl, function( data ) {
                            // alert( "Data Loaded: " + data );
                            for(var i=0;i<data.length;i++){
                            $("<li><a  onclick=displayOperator('"+data[i].id +"') >" + data[i].name + "</a></li>").appendTo('#operators');
                              console.log("operators loaded");
                                } 
                          });
                }


            function populatepage(){
                          $.get( operatorsUrl, function( data ) {
                            // alert( "Data Loaded: " + data );
                           
                             
                            $(" <h1 class='page-header'>"+operatorName+"</h1>").appendTo('#operatorName');
                              console.log("Title loaded");

                              
                                
                          });
                }

    populatepage();
    getoperators();
  




      console.log('operator '+operatorVehicle(operatorData,'Lorry')+' audit'+operatorVehicle(auditData,'Lorry'));

    var ctx = $('#barChart');
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Lorry", "Van", "Motorcycle", "Car"],
        datasets: [{
            label: '# of Vehicles',
            data: [ operatorVehicle(operatorData,'Lorry'), operatorVehicle(operatorData,'Van'), operatorVehicle(operatorData,'Motorcycle'),operatorVehicle(operatorData,'Car')],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(75, 192, 192, 1)',
                
                
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 99, 132, 0.2)',

            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
        
       
      // Pie chart


    var myChart = new Chart($('#pieChart'), {
    type: 'pie',
    data: {
    labels: ["Lorry", "Van",  "Motorcycle",  "Car"],
        datasets: [{
            label: '# of Vehicles',
            data: [  operatorVehicle(auditData,'Lorry'),operatorVehicle(auditData,'Van'), operatorVehicle(auditData,'Motorcycle'),operatorVehicle(auditData,'Car')],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
               
                'rgba(255, 206, 86, 0.2)',
             
                'rgba(75, 192, 192, 0.2)',
              
                'rgba(75, 192, 192, 1)',
              
                
            ]
           
        }]
    }
    
});
       
    

    }

var Lorry = (function() {

  var LorryNum;
      $.get( operatorUrl, function( data ) {

        operatorData=data;
          $.get( auditUrl, function( data ) {

                   auditData=data;
                    plotter();
          })

        
     })
})();
